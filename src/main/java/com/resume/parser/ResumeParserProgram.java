package com.resume.parser;

import static gate.Utils.stringFor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.ToXMLContentHandler;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.util.GateException;
import gate.util.Out;

@Component
public class ResumeParserProgram {
	public File parseToHTMLUsingApacheTikka(String file) throws IOException, SAXException, TikaException {
		String ext = FilenameUtils.getExtension(file);
		String outputFileFormat = "";
		if (ext.equalsIgnoreCase("html") | ext.equalsIgnoreCase("pdf") | ext.equalsIgnoreCase("doc")
				| ext.equalsIgnoreCase("docx")) {
			outputFileFormat = ".html";
		} else if (ext.equalsIgnoreCase("txt") | ext.equalsIgnoreCase("rtf")) {
			outputFileFormat = ".txt";
		} else {
			System.out.println("Input format of the file " + file + " is not supported.");
			return null;
		}
		String OUTPUT_FILE_NAME = FilenameUtils.removeExtension(file) + outputFileFormat;
		ContentHandler handler = new ToXMLContentHandler();
		InputStream stream = new FileInputStream(file);
		AutoDetectParser parser = new AutoDetectParser();
		Metadata metadata = new Metadata();
		try {
			parser.parse(stream, handler, metadata);
			FileWriter htmlFileWriter = new FileWriter(OUTPUT_FILE_NAME);
			htmlFileWriter.write(handler.toString());
			htmlFileWriter.flush();
			htmlFileWriter.close();
			return new File(OUTPUT_FILE_NAME);
		} finally {
			stream.close();
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject loadGateAndAnnie(File file) throws GateException, IOException {
		System.setProperty("gate.site.config", System.getProperty("user.dir") + "/GATEFiles/gate.xml");
		if (Gate.getGateHome() == null)
			Gate.setGateHome(new File(System.getProperty("user.dir") + "/GATEFiles"));
		if (Gate.getPluginsHome() == null)
			Gate.setPluginsHome(new File(System.getProperty("user.dir") + "/GATEFiles/plugins"));
		Gate.init();

		Annie annie = new Annie();
		annie.initAnnie();

		Corpus corpus = Factory.newCorpus("Annie corpus");
		URL u = file.toURI().toURL();
		FeatureMap params = Factory.newFeatureMap();
		params.put("sourceUrl", u);
		params.put("preserveOriginalContent", true);
		params.put("collectRepositioningInfo", true);
		Out.prln("Creating doc for " + u);
		Document resume = (Document) Factory.createResource("gate.corpora.DocumentImpl", params);

		corpus.add(resume);

		System.out.println("***************** BEFORE ANNIE ****************");

		System.out.println(corpus.get(0));

		System.out.println("***************** END BEFORE ANNIE ****************");

		annie.setCorpus(corpus);
		annie.execute();

		System.out.println("***************** AFTER ANNIE ****************");

		System.out.println(corpus.get(0));

		System.out.println("***************** END AFTER ANNIE ****************");


		Iterator iter = corpus.iterator();
		JSONObject parsedJSON = new JSONObject();
		Out.prln("Started parsing...");
		JSONObject profileJSON = null;
		if (iter.hasNext()) {
			profileJSON = new JSONObject();
			Document doc = (Document) iter.next();
			AnnotationSet defaultAnnotSet = doc.getAnnotations();

			AnnotationSet curAnnSet;
			Iterator it;
			Annotation currAnnot;

			curAnnSet = defaultAnnotSet.get("EmailFinder");
			System.out.println(curAnnSet.iterator());
			if (curAnnSet.iterator().hasNext()) {
				currAnnot = curAnnSet.iterator().next();
				String email = stringFor(doc, currAnnot);
				if (email != null && email.length() > 0) {
					parsedJSON.put("email", email);
				}
			}
		}
		return parsedJSON;
	}
}
